from dataclasses import fields, is_dataclass, MISSING, asdict
from typing import TypeVar, Type, cast

T = TypeVar('T')

def partial(cls: Type[T]) -> Type[T]:
    if not is_dataclass(cls):
        raise TypeError("partial decorator can only be used on dataclasses")

    class PartialWrapper(cls):
        def __init__(self, *args, **kwargs):
            # Collect fields from the class and all its parent classes
            self._cls_fields = {}
            for c in cls.mro()[::-1]:
                if is_dataclass(c):
                    self._cls_fields.update({f.name: f for f in fields(c)})
            cls_field_names = list(self._cls_fields.keys())

            # Check for unexpected keyword arguments
            for key in kwargs:
                if key not in cls_field_names:
                    raise TypeError(f"__init__() got an unexpected keyword argument '{key}'")

            # Set default values for missing fields
            cls_defaults = {f.name: (f.default if f.default is not MISSING else f.default_factory()) for f in self._cls_fields.values() if f.default is not MISSING or f.default_factory is not MISSING}

            # Assign positional args by field name
            positional_args = dict(zip(cls_field_names, args))

            # Combine all fields with defaults, positional args, and keyword args
            combined_args = {**cls_defaults, **positional_args, **kwargs}

            # Set attributes directly without calling super().__init__
            for name, value in combined_args.items():
                setattr(self, name, value)

            # Keep track of explicitly set fields
            self._explicitly_set_fields = set(positional_args.keys()) | set(kwargs.keys())

        def __getattr__(self, name):
            if name in self._cls_fields and name not in self._explicitly_set_fields:
                return None
            if hasattr(super(), name):
                return getattr(super(), name)
            raise AttributeError(f"'{type(self).__name__}' object has no attribute '{name}'")

        def to_dict(self, only_init: bool = False, filter_none: bool = False):
            result_dict = asdict(self)

            if only_init:
                # Keep only fields that were explicitly set
                result_dict = {k: v for k, v in result_dict.items() if k in self._explicitly_set_fields}

            if filter_none:
                # Filter out keys with None values
                result_dict = {k: v for k, v in result_dict.items() if v is not None}

            return result_dict

    PartialWrapper.__name__ = cls.__name__

    return cast(Type[T], PartialWrapper)
