
# Installation
```sh
pip install git+https://gitlab.com/roadsync-public/class-utilities.git@v1.0.0
```

# Using the `@partial` Decorator
This powerful decorator allows you to create partial versions of @dataclasses without all required fields set.
The generated partial class can accept anything the original can.

```python
from dataclasses import dataclass
from class_utilities import partial

@dataclass
class Todo:
    task: str
    completed: bool = False

@partial
class PartialTodo(Todo):
    pass

# you can instantiate all or any number of the original fields
todo = PartialTodo(task='Buy milk')
assert todo.task == 'Buy milk'
assert todo.completed == False

# only_init=True ignores any field not set in the constructor
todo = PartialTodo(task='Buy milk')
assert todo.to_dict(only_init=True) == {'task': 'Buy milk'}

# filter_none=True removes all None values
todo = PartialTodo()
assert todo.to_dict(filter_none=True) == {'completed': False}
```