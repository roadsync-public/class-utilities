from typing import Optional
import pytest
from dataclasses import dataclass, field
from class_utilities import partial  # Assuming your partial decorator is in class_utilities
from collections import namedtuple
import json

# Define a namedtuple for testing
TodoRecord = namedtuple('TodoRecord', 'task completed')

# Define Todo-themed test dataclasses
@dataclass
class Todo:
    task: str
    completed: bool = False

@partial
class PartialTodo(Todo):
    pass

@dataclass
class TodoWithDefaults:
    task: str = 'default task'
    completed: bool = True

@partial
class PartialTodoWithDefaults(TodoWithDefaults):
    pass

# Test Todo with a nested dataclass
@dataclass
class NestedDetails:
    description: str

@dataclass
class TodoWithDetails:
    details: NestedDetails

@partial
class PartialTodoWithDetails(TodoWithDetails):
    pass

# Test Todo with a mutable default value (e.g., tags)
@dataclass
class TodoWithTags:
    tags: list = field(default_factory=list)

@partial
class PartialTodoWithTags(TodoWithTags):
    pass

@dataclass
class ExtendedTodo(Todo):
    priority: int = 1

@partial
class PartialExtendedTodo(ExtendedTodo):
    pass

@dataclass
class TypedTodo:
    task: str
    deadline: Optional[str] = None

@partial
class PartialTypedTodo(TypedTodo):
    pass

# Define individual test functions
def test_partial_todo_initialization():
    todo = PartialTodo(task='Buy milk')
    assert todo.task == 'Buy milk'
    assert not todo.completed

def test_partial_todo_attribute_access():
    todo = PartialTodo(task='Buy milk')
    with pytest.raises(AttributeError):
        _ = todo.nonexistent_field

def test_partial_todo_to_dict_only_init():
    todo = PartialTodo(task='Buy milk', completed=True)
    expected = {'task': 'Buy milk', 'completed': True}
    assert todo.to_dict() == expected

def test_partial_todo_to_dict_only_init_after():
    todo = PartialTodo()
    todo.task = 'Buy milk'
    expected = {}
    assert todo.to_dict(only_init=True) == expected

def test_partial_todo_to_dict_filtered_fields():
    todo = PartialTodo(task='Buy milk')
    expected = {'task': 'Buy milk'}
    assert todo.to_dict(only_init=True) == expected

def test_partial_todo_to_dict_filter_none():
    todo = PartialTodo(task='Buy milk')
    expected = {'task': 'Buy milk', 'completed': False}
    assert todo.to_dict(filter_none=True) == expected

def test_partial_todo_with_default_values():
    todo = PartialTodoWithDefaults()
    assert todo.task == 'default task'
    assert todo.completed

def test_partial_todo_with_nested_dataclass():
    details = NestedDetails(description='Grocery shopping')
    todo = PartialTodoWithDetails(details=details)
    assert todo.details.description == 'Grocery shopping'

def test_partial_todo_with_mutable_default():
    todo = PartialTodoWithTags()
    todo.tags.append('urgent')
    assert todo.tags == ['urgent']

def test_partial_todo_with_namedtuple_unpacking():
    """Test initializing a partial Todo with namedtuple unpacking."""
    todo_record = TodoRecord(task='Write tests', completed=True)
    todo = PartialTodo(**todo_record._asdict())
    assert todo.task == 'Write tests'
    assert todo.completed

def test_partial_todo_with_tuple_unpacking():
    """Test initializing a partial Todo with tuple unpacking."""
    todo_tuple = ('Review PR', False)
    todo = PartialTodo(*todo_tuple)
    assert todo.task == 'Review PR'
    assert not todo.completed

def test_partial_todo_with_incomplete_record():
    """Test initializing a partial Todo with an incomplete record."""
    todo_record = TodoRecord(task='Attend meeting', completed=None)
    todo = PartialTodo(**todo_record._asdict())
    assert todo.task == 'Attend meeting'
    assert todo.completed is None

def test_partial_todo_with_additional_fields():
    """Test initializing a partial Todo with additional fields in the record should raise an error."""
    todo_record = {'task': 'Fix bug', 'completed': False, 'priority': 'High'}
    with pytest.raises(TypeError) as excinfo:
        todo = PartialTodo(**todo_record)
    
    assert "got an unexpected keyword argument 'priority'" in str(excinfo.value)

def test_partial_todo_json_serialization():
    todo = PartialTodo(task='Write code')
    todo_json = json.dumps(todo.to_dict())
    deserialized_todo_json = json.loads(todo_json)
    expected_dict = {"task": "Write code", "completed": False}
    assert deserialized_todo_json == expected_dict

def test_partial_todo_json_serialization_filter_none():
    todo = PartialTodo(task='Write code')
    todo_json = json.dumps(todo.to_dict(filter_none=True))
    expected_json = '{"task": "Write code", "completed": false}'
    assert todo_json == expected_json

def test_partial_todo_json_serialization_only_init():
    todo = PartialTodo(task='Write code')
    todo_json = json.dumps(todo.to_dict(only_init=False))
    expected_json = '{"task": "Write code", "completed": false}'
    assert todo_json == expected_json

def test_partial_todo_json_deserialization():
    json_data = '{"task": "Review PR", "completed": true}'
    data = json.loads(json_data)
    todo = PartialTodo(**data)
    assert todo.task == 'Review PR' and todo.completed

def test_partial_todo_with_details_json_serialization():
    details = NestedDetails(description='Meeting at 10 AM')
    todo = PartialTodoWithDetails(details=details)
    todo_json = json.dumps(todo.to_dict())
    expected_json = '{"details": {"description": "Meeting at 10 AM"}}'
    assert todo_json == expected_json

def test_partial_initialization_with_partial_fields():
    todo = PartialTodo(task='Review code')
    assert todo.task == 'Review code'
    assert todo.completed is False  # Assuming False is the default value for 'completed'

def test_partial_initialization_with_no_fields():
    todo = PartialTodo()
    assert todo.task is None
    assert todo.completed is False  # Assuming False is the default value for 'completed'

def test_partial_todo_json_round_trip():
    original_todo = PartialTodo(task='Fix bug', completed=True)
    todo_json = json.dumps(original_todo.to_dict())
    deserialized_todo = PartialTodo(**json.loads(todo_json))
    assert deserialized_todo == original_todo

def test_partial_todo_update_instance():
    todo = PartialTodo(task='Prepare report')
    todo.completed = True
    assert todo.completed is True

def test_partial_optional_todo_update_instance():
    todo = PartialTodo(completed=True)
    todo.task = 'Prepare report'
    assert todo.completed is True
    assert todo.task == 'Prepare report'

def test_partial_complete_todo_update_instance():
    todo = PartialTodo()
    todo.task = 'Prepare report'
    todo.completed = True
    assert todo.completed is True
    assert todo.task == 'Prepare report'

def test_partial_todo_invalid_field():
    with pytest.raises(TypeError):
        _ = PartialTodo(unknown_field='some value')

def test_partial_todo_with_default_factory():
    todo = PartialTodoWithTags()
    assert todo.tags == []  # Assuming the default factory for 'tags' is list
    todo.tags.append('urgent')
    assert todo.tags == ['urgent']

def test_partial_todo_json_serialization_with_default_values():
    todo = PartialTodo(task='Fix bug')
    todo_json = json.dumps(todo.to_dict())
    expected_json = '{"task": "Fix bug", "completed": false}'
    assert todo_json == expected_json

def test_partial_initialization_only_init_unset():
    todo = PartialTodo()
    assert todo.task is None
    assert todo.completed == False  # Assuming default is False

def test_partial_todo_with_details_json_serialization_unset_fields():
    todo = PartialTodoWithDetails(details=None)
    todo_json = json.dumps(todo.to_dict())
    expected_json = '{"details": null}'
    assert todo_json == expected_json

def test_partial_instance_equality():
    todo1 = PartialTodo(task='Attend meeting')
    todo2 = PartialTodo(task='Attend meeting')
    assert todo1 == todo2

def test_partial_todo_to_dict_with_unset_fields_and_filter_none():
    todo = PartialTodo(task=None)
    expected = {'completed': False}
    assert todo.to_dict(filter_none=True) == expected

def test_partial_extended_todo():
    todo = PartialExtendedTodo(task='Review PR', priority=2)
    assert todo.task == 'Review PR'
    assert todo.completed == False
    assert todo.priority == 2

def test_partial_typed_todo():
    todo = PartialTypedTodo(task='Finish report')
    assert todo.task == 'Finish report'
    assert todo.deadline is None

@partial
@partial
class DoublePartialTodo(Todo):
    pass

def test_double_partial_todo():
    todo = DoublePartialTodo(task='Plan meeting')
    assert todo.task == 'Plan meeting'
    assert todo.completed == False

def test_partial_mixed_args_initialization():
    todo = PartialTodo('Submit report', completed=True)
    assert todo.task == 'Submit report'
    assert todo.completed == True

def test_partial_inherited_with_additional_fields():
    @dataclass
    class BaseTodo:
        task: str

    @dataclass
    class ExtendedTodo(BaseTodo):
        priority: int

    @partial
    class PartialExtendedTodo(ExtendedTodo):
        pass

    todo = PartialExtendedTodo(task='Check emails', priority=3)
    assert todo.task == 'Check emails'
    assert todo.priority == 3

@dataclass
class TodoWithMethod:
    task: str
    completed: bool = False

    def is_completed(self) -> bool:
        return self.completed

@partial
class PartialTodoWithMethod(TodoWithMethod):
    pass

def test_partial_todo_with_class_method():
    todo = PartialTodoWithMethod(task='Refactor code')
    assert not todo.is_completed()

